import QtQuick 2.12
import QtQuick.Controls 1.4 as QC1
import QtQuick.Controls 2.12
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.12
import QtQuick.Window 2.12
import BarChart.UI 1.0

/*!
    \qmltype main
    \ingroup UI
    \inqmlmodule BarChart.UI
    \since 1.0
    \inherits Window
    \brief Главное окно тестовой программы
*/
Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Распределение самых часто встречающихся слов")

    /*!
        \qmlfunction string urlToAbsolutePath
        преобразует url вида "file:///C:/Users/user/file.txt" в строку C:/Users/user/file.txt
    */
    function urlToAbsolutePath(url) {
        var path = url.toString();
        path = path.replace(/^(file:\/{3})|(qrc:\/{2})|(http:\/{2})/,"")
        return decodeURIComponent(path)
    }
    WordDistributionModel {
        id: distributionModel
        onProgressChanged: progressBar.value = percent / 100.0
        onError: {
            errDlg.text = msg
            errDlg.open()
        }
    }

    ColumnLayout {
        anchors.fill: parent
        id: mainLayout
        RowLayout {
            id: fileRow
            Layout.fillWidth: true
            Layout.leftMargin: spacing
            Layout.rightMargin: spacing
            Layout.topMargin: spacing
            TextField {
                id: filePathField
                Layout.fillWidth: true
                placeholderText: qsTr("Выберите текстовый файл для подсчёта слов")
            }
            Button {
                text: qsTr("Выбрать")
                onClicked: fileDialog.open()
            }
        }
        BarChart {
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.leftMargin: mainLayout.spacing
            Layout.rightMargin: mainLayout.spacing
            Layout.bottomMargin: 10
            model: distributionModel
        }
        RowLayout {
            id: progressRow
            Layout.fillWidth: true
            Layout.leftMargin: spacing
            Layout.rightMargin: spacing
            Layout.bottomMargin: spacing
            ProgressBar {
                id: progressBar
                Layout.fillWidth: true
            }
            Button {
                text: qsTr("Начать расчёт")
                enabled: !distributionModel.busy
                onClicked: distributionModel.calculate(filePathField.text, minWordLen.value)
            }
            Text {
                id: minWordLenTxt
                text: qsTr("Мин. длинна слова:")
            }
            QC1.SpinBox {
                id: minWordLen
                enabled: !distributionModel.busy
            }
        }
    }
    FileDialog {
        id: fileDialog
        title: qsTr("Файл для подсчёта самых часто встречающихся слов")
        nameFilters: [qsTr("Текстовые файлы (*.txt)")]
        onAccepted: filePathField.text = urlToAbsolutePath(fileUrl)
    }
    MessageDialog {
        id: errDlg
        standardButtons: MessageDialog.Ok
    }
}
