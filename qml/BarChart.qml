import QtQuick 2.12
import QtQuick.Controls 2.12
import BarChart.UI 1.0

/*!
    \qmltype BarChart
    \ingroup UI
    \inqmlmodule BarChart.UI
    \since 1.0
    \inherits Rectangle
    \brief Гистограмма
*/
Rectangle {
    id: chart
    /*!
        \qmlproperty BarChart::model
        модель распределения количества слов
    */
    property WordDistributionModel model
    border.width: 1
    Row {
        id: barsRow
        spacing: 2
        /*!
            \qmlproperty barsRow::heighestBarValue
            значение самого высокого столбца
        */
        property int heighestBarValue: 0
        /*!
            \qmlproperty barsRow::pxPerWord
            разрешение гистограммы по высоте, пикс/слово.
            Можно редактировать, сколько может занимать столбец в % от высоты гистограммы
        */
        property double pxPerWord: heighestBarValue == 0 ? 0 : (chart.height * 0.9 / heighestBarValue)
        Repeater {
            id: barsRep
            anchors.fill: parent
            model: chart.model
            delegate: Column {
                y: chart.height - rect.height
                id: col
                width: barsRep.count == 0 ? 0 : ( (chart.width / barsRep.count) - barsRow.spacing)
                Rectangle {
                    id: rect
                    color: "green"
                    height: model["count"] * barsRow.pxPerWord
                    width: col.width
                    Label {
                        text: model["count"]
                        anchors.centerIn: parent
                    }
                }
                Text {
                    horizontalAlignment: Qt.AlignHCenter
                    text: model["word"]
                    width: col.width
                }
            }
        }
    }
    Connections {
        target: model
        function onMaximumCalculated(maxWordCount) {
            barsRow.heighestBarValue = maxWordCount
        }
    }
}
