#include "basefileparser.h"
#include <QRegularExpression>

void BaseFileParser::parse(const QString &path, quint32 minWordLength)
{
    if (path.isEmpty())
    {
        emit error( tr("Пустое имя файла.") );
        return;
    }
    emit progressChanged(0, 0);
    m_sortedWordCountDistribution.clear();
    QString plainText;
    try
    {
        plainText = readPlainText(path);
    }
    catch(const std::exception& e)
    {
        emit error(e.what());
        return;
    }

    emit progressChanged(0, 20);

    //разбор по словам. Можно было бы сильно оптимальнее в один подход, если находу заполнять хэш
    //и не проверять прописные/строчные буквы
    //но в этом задании ограничений по быстродействию нет, а у меня ограничение по времени разработки есть, поэтому так)
    const auto words = plainText.split(QRegularExpression(QStringLiteral("[\\[\\] ,.;!-?\\s]")), Qt::SkipEmptyParts);

    if(words.size() < wordsCount())
    {
        emit error( tr("Количество слов в выбранном файле меньше минимального") );
        return;
    }

    emit progressChanged(0, 40);

    //Подсчёт количества каждого слова
    QHash<QString, int> wordsCountTable;
    for(const auto& word : words)
    {
        if(word.length() >= static_cast<int>(minWordLength))
            ++wordsCountTable[word.toLower()];
    }

    emit progressChanged(0, 60);

    //Выбор топ-wordsCount() самых часто встречающихся слов
    do
    {
        auto maxElt = std::max_element(wordsCountTable.begin(), wordsCountTable.end());
        if ( Q_UNLIKELY(m_sortedWordCountDistribution.isEmpty()) )
            emit maxWordCountCalculated(maxElt.value());

        m_sortedWordCountDistribution.append({maxElt.key(), static_cast<quint32>(maxElt.value())});
        wordsCountTable.erase(maxElt);
    }
    while (m_sortedWordCountDistribution.size() < wordsCount());
    emit progressChanged(0, 80);

    //сортировка по алфавиту
    std::sort(m_sortedWordCountDistribution.begin(), m_sortedWordCountDistribution.end(), [](const auto& lhs, const auto& rhs) {
        return lhs.word < rhs.word;
    });

    emit progressChanged(words.size(), 100);
}
