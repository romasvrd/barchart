#ifndef WORDDISTRIBUTIONMODEL_H
#define WORDDISTRIBUTIONMODEL_H

#include <QAbstractListModel>
#include <memory>
#include <QThread>

class TxtFileParser;
struct WordCount;

/*!
 * \brief Модель распределения слов по их количеству
 */
class WordDistributionModel : public QAbstractListModel
{
    Q_OBJECT

public:
    enum Roles
    {
        WordRole = Qt::UserRole,    ///< слово
        CountRole                   ///< количество вхождений
    };

    WordDistributionModel(QObject* parent = nullptr);
    ~WordDistributionModel();

    // QAbstractListModel interface
    int rowCount(const QModelIndex &parent = QModelIndex()) const final;
    QVariant data(const QModelIndex &index, int role) const final;
    QHash<int, QByteArray> roleNames() const final;

    Q_PROPERTY(bool busy READ busy WRITE setBusy NOTIFY busyChanged)    ///< занят (идет процесс расчёта)
    bool busy() const {return m_busy;}
    void setBusy(bool val) { if(m_busy==val) return; m_busy = val; emit busyChanged(); }

    /*!
     * \brief Расчитать распределение
     * \param absolutePath путь к файлу для расчёта
     * \param minWordLength минимально учитываемая длина файла
     */
    Q_INVOKABLE void calculate(const QString& absolutePath, quint32 minWordLength);

signals:
    void error(const QString& msg);
    void needCalculate(const QString& absolutePath, quint32 minWordLength);
    void progressChanged(int cnt, int percent);
    void maximumCalculated(int maxWordCount);
    void busyChanged();

private:
    void reportError(const QString& msg);
    QVector<WordCount> m_wordDistribution;
    std::unique_ptr<TxtFileParser> m_parser;
    bool m_busy = false;

    QThread m_parserThread;
};

#endif //WORDDISTRIBUTIONMODEL_H
