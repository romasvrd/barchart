#include "worddistributionmodel.h"
#include "txtfileparser.h"

WordDistributionModel::WordDistributionModel(QObject *parent)
    : QAbstractListModel(parent)
{
    m_parser = std::make_unique<TxtFileParser>(parent);
    m_parser->moveToThread(&m_parserThread);

    connect(this, &WordDistributionModel::needCalculate, m_parser.get(), &TxtFileParser::parse);
    connect(m_parser.get(), &TxtFileParser::progressChanged, this, [this](int cnt, int percent) {
        emit progressChanged(cnt, percent);
        if (percent == 100)
        {
            beginResetModel();
            m_wordDistribution = m_parser->distribution();
            endResetModel();
            setBusy(false);
        }
    });
    connect(m_parser.get(), &TxtFileParser::maxWordCountCalculated, this,
            std::bind(&WordDistributionModel::maximumCalculated, this, std::placeholders::_1));
    connect(m_parser.get(), &TxtFileParser::error, this, [this] (const QString& msg){
        setBusy(false);
        reportError(msg);
    });

    m_parserThread.start();
}

WordDistributionModel::~WordDistributionModel()
{
    m_parserThread.quit();
    m_parserThread.wait();
}

int WordDistributionModel::rowCount(const QModelIndex &) const
{
    return m_wordDistribution.size();
}

QVariant WordDistributionModel::data(const QModelIndex &index, int role) const
{
    Q_ASSERT(index.isValid());
    const auto& wordCnt = m_wordDistribution.at(index.row());
    switch (role)
    {
    case WordRole:
        return wordCnt.word;
    case CountRole:
        return wordCnt.count;
    default:
        Q_UNREACHABLE();
    }
}

QHash<int, QByteArray> WordDistributionModel::roleNames() const
{
    static const QHash<int, QByteArray> roles = {
        {WordRole, QByteArrayLiteral("word")},
        {CountRole, QByteArrayLiteral("count")}
    };
    return roles;
}

void WordDistributionModel::calculate(const QString &absolutePath, quint32 minWordLength)
{
    if (busy())
        return;
    if (absolutePath.isEmpty())
    {
        reportError( tr("Задано пустое имя файла"));
        return;
    }
    setBusy(true);
    emit needCalculate(absolutePath, minWordLength);
}

void WordDistributionModel::reportError(const QString &msg)
{
    emit error( tr("Ошибка расчёта. %1").arg(msg));
}
