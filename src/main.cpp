#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QTextCodec>
#include "txtfileparser.h"
#include "worddistributionmodel.h"

void registerTypes()
{
    qmlRegisterType<TxtFileParser>("BarChart.UI", 1, 0, "TxtFileParser");
    qmlRegisterType<WordDistributionModel>("BarChart.UI", 1, 0, "WordDistributionModel");
}

int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    registerTypes();

    const QUrl url(QStringLiteral("qrc:/qml/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
