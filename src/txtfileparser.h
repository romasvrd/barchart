#ifndef TXTFILEPARSER_H
#define TXTFILEPARSER_H

#include "basefileparser.h"

/*!
 * \brief парсер текстовых файлов (*.txt)
 */
class TxtFileParser : public BaseFileParser
{
    Q_OBJECT

public:
    TxtFileParser(QObject* parent = nullptr);

protected:
    virtual QString readPlainText(const QString& path) final;
};

#endif //TXTFILEPARSER_H
