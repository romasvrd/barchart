#include "txtfileparser.h"
#include <QFile>
#include <QTextStream>

TxtFileParser::TxtFileParser(QObject *parent)
    : BaseFileParser(parent)
{

}

QString TxtFileParser::readPlainText(const QString &path)
{
    if(!QFile::exists(path))
        throw std::exception( tr("Файл %1 не найден").arg(path).toStdString().c_str());

    QFile txt(path);
    if(!txt.open(QIODevice::ReadOnly))
        throw std::exception( tr("Не удалось открыть файл %1. %2").arg(path, txt.errorString()).toStdString().c_str() );

    QTextStream in(&txt);

    const auto plainText = in.readAll();
    txt.close();

    return plainText;
}
