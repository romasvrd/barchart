#ifndef BASEFILEPARSER_H
#define BASEFILEPARSER_H

#include <QObject>
#include <QHash>


struct WordCount
{
    QString word;
    quint32 count;
};

class BaseFileParser : public QObject
{
    Q_OBJECT

public:
    BaseFileParser(QObject* parent = nullptr) :
        QObject(parent) {}

    virtual ~BaseFileParser() = default;

    Q_PROPERTY(int wordsCount READ wordsCount WRITE setWordsCount NOTIFY wordsCountChanged);
    int wordsCount() const { return m_wordsCount; }
    void setWordsCount(int val) {if (m_wordsCount != val) return; m_wordsCount = val; emit wordsCountChanged();}

    QVector<WordCount> distribution() {return m_sortedWordCountDistribution;}

public slots:
    void parse(const QString& path, quint32 minWordLength);

signals:
    void progressChanged(int cnt, int percent);
    void wordsCountChanged();
    void maxWordCountCalculated(int maxWordCount);
    void error(const QString& what);

protected:
    QVector<WordCount> m_sortedWordCountDistribution;
    virtual QString readPlainText(const QString& path) = 0;

private:
    int m_wordsCount = 15;  ///< количество слов для расчёта
};
#endif //BASEFILEPARSER_H
